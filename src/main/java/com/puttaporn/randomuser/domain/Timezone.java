package com.puttaporn.randomuser.domain;

import java.io.Serializable;

public class Timezone implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String offset;
	private String description;
	public String getOffset() {
		return offset;
	}
	public void setOffset(String offset) {
		this.offset = offset;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	
}
