package com.puttaporn.randomuser.domain.service;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.puttaporn.randomuser.application.response.RandomUserResponse;


@Component
public class RandomUserService {

	@Cacheable(value = "response", key = "#seed", unless = "#result==null")
	public RandomUserResponse  randomUser(String seed) {
		System.out.println("Enter randomUser seed ="+seed);
		RandomUserResponse response = new RandomUserResponse();
		
		String url = "https://randomuser.me/api/";
		
		if(seed!=null&&!seed.equals("")) {
			url = url+"?seed="+seed;
		}
		RestTemplate restTemplate = new RestTemplate();
		String responseJson = restTemplate.getForObject(url, String.class); 
		
		 GsonBuilder builder = new GsonBuilder(); 
	      builder.setPrettyPrinting();
		 Gson gson = builder.create();
		 response = gson.fromJson(responseJson, RandomUserResponse.class);
		return response;
	}
}
