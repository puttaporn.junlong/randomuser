package com.puttaporn.randomuser;

import org.springframework.boot.SpringApplication;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@EnableCaching
@SpringBootApplication
public class RandomUserApplication {

	public static void main(String[] args) {
		SpringApplication.run(RandomUserApplication.class, args);
	}

}
