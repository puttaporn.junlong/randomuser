package com.puttaporn.randomuser.application.request;

public class RandomUserRequest {
	private String seed;

	public String getSeed() {
		return seed;
	}

	public void setSeed(String seed) {
		this.seed = seed;
	}
	
	
}
