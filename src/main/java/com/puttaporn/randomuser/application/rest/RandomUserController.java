package com.puttaporn.randomuser.application.rest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.puttaporn.randomuser.application.request.RandomUserRequest;
import com.puttaporn.randomuser.application.response.RandomUserResponse;
import com.puttaporn.randomuser.domain.service.RandomUserService;

@RestController
@RequestMapping("/")
public class RandomUserController {

	@Autowired
	private RandomUserService randomUserService;

	@GetMapping(value = "/randomUser", produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {
			MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody RandomUserResponse randomUser(HttpServletRequest request,
			@RequestBody RandomUserRequest requestMsgModel, HttpServletResponse response) throws Exception {

		RandomUserResponse responseModel = randomUserService.randomUser(requestMsgModel.getSeed());

		return responseModel;

	}
}
