package com.puttaporn.randomuser.application.response;

import java.io.Serializable;
import java.util.List;

import com.puttaporn.randomuser.domain.Info;
import com.puttaporn.randomuser.domain.User;

public class RandomUserResponse implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<User> results;
	private Info info;
	public List<User> getResults() {
		return results;
	}
	public void setResults(List<User> results) {
		this.results = results;
	}
	public Info getInfo() {
		return info;
	}
	public void setInfo(Info info) {
		this.info = info;
	}
	
	
	
	
	
	
	
}
